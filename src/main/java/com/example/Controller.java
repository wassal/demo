package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {


    HelloApplicationService helloApplicationService;

    @Autowired
    public Controller(HelloApplicationService helloApplicationService) {
        this.helloApplicationService = helloApplicationService;
    }

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public ResponseEntity test() {
        return ResponseEntity.ok().body(helloApplicationService.test());
    }
}
